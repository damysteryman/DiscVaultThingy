namespace DiscVaultThingy
{
    public class Region
    {
        private long _id;
        private string _name;

        public long ID { get => _id; set { _id = value; Modified = true; } }
        public string Name { get => _name; set { _name = value; Modified = true; } }
        public bool Modified { get; private set; }

        public Region(string _name, long _id = 0)
        {
            ID = _id;
            Name = _name;

            Modified = _id == 0;
        }
    }
}