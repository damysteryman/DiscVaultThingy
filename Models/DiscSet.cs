namespace DiscVaultThingy
{
    public class DiscSet
    {
        private long _id;
        private string _name;
        private long _discCount;
        private string _source;
        private Region _region;
        private Series _series;
        public long ID { get => _id; set { _id = value; Modified = true; } }
        public string Name { get => _name; set { _name = value; Modified = true; } }
        public long DiscCount { get => _discCount; set { _discCount = value; Modified = true; } }
        public string Source { get => _source; set { _source = value; Modified = true; } }
        public Region Region { get => _region; set { _region = value; Modified = true; } }
        public Series Series { get => _series; set { _series = value; Modified = true; } }

        public bool Modified { get; private set; }

        public DiscSet(string _name, long _discCount, string _source, Region _region, Series _series, long _id = 0)
        {
            ID = _id;
            Name = _name;
            DiscCount = _discCount;
            Source = _source;
            Region = _region;
            Series = _series;

            Modified = _id == 0;
        }

        public override string ToString()
        {
            return $"{Series.Name}|{Name}";
        }
    }
}