namespace DiscVaultThingy
{
    public class Disc
    {
        private long _id;
        private long _vaultSlotNo;
        private string _name;
        private string _friendlyName;
        private DiscType _type;
        private DiscSet _set;
        private long _discNo;
        private bool _isBad;
        private string _comment;

        public long ID { get => _id; private set { _id = value; Modified = true; } }
        public long VaultSlotNo { get => _vaultSlotNo; set { _vaultSlotNo = value; Modified = true; } }
        public string Name { get => _name; set { _name = value; Modified = true; } }
        public string FriendlyName { get => _friendlyName; set { _friendlyName = value; Modified = true; } }
        public DiscType Type { get => _type; set { _type = value; Modified = true; } }
        public DiscSet Set { get => _set; set { _set = value; Modified = true; } }
        public long DiscNo { get => _discNo; set { _discNo = value; Modified = true; } }
        public bool IsBad { get => _isBad; set { _isBad = value; Modified = true; } }
        public string Comment { get => _comment; set { _comment = value; Modified = true; } }

        public bool Modified { get; private set; }

        public Disc(long _vaultSlotNo, string _name, string _friendlyName, DiscType _type, DiscSet _set, long _discNo, bool _isBad, string _comment, long _id = 0)
        {
            ID = _id;
            VaultSlotNo = _vaultSlotNo;
            Name = _name;
            FriendlyName = _friendlyName;
            Type = _type;
            Set = _set;
            DiscNo = _discNo;
            IsBad = _isBad;
            Comment = _comment;

            Modified = _id == 0;
        }
    }
}