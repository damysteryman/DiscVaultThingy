using System;
using Gtk;

namespace DiscVaultThingy
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            var app = new Application("org.DiscVaultThingy.DiscVaultThingy", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            var win = new MainWindow();
            app.AddWindow(win);
            // win.Maximize();
            win.Show();
            Application.Run();
        }
    }
}
