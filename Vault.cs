using System.Collections.Generic;
using System.Data;

namespace DiscVaultThingy
{
    public class Vault
    {
        private Data _data = null;

        public List<Disc> Discs { get; set; }
        public List<DiscSet> DiscSets { get; set; }
        public List<DiscType> DiscTypes { get; set; }
        public List<Region> Regions { get; set; }
        public List<Series> Series { get; set; }

        public Vault(string path)
        {
            _data = new Data(path);
        }

        public bool LoadData()
        {
            return
            LoadDiscTypes() &&
            LoadRegions() &&
            LoadSeries() &&
            LoadDiscSets() &&
            LoadDiscs();
        }

        public bool LoadDiscTypes()
        {
            if (_data == null)
                return false;

            DataTable dt = _data.GetTable("DiscType");
            DiscTypes = new List<DiscType>();
            foreach (DataRow r in dt.Rows)
                DiscTypes.Add(new DiscType((string)r["Name"], (long)r["id"]));

            return true;
        }

        public bool LoadRegions()
        {
            if (_data == null)
                return false;

            DataTable dt = _data.GetTable("Region");
            Regions = new List<Region>();
            foreach (DataRow r in dt.Rows)
                Regions.Add(new Region((string)r["Name"], (long)r["id"]));

            return true;
        }

        public bool LoadSeries()
        {
            if (_data == null)
                return false;

            DataTable dt = _data.GetTable("Series");
            Series = new List<Series>();
            foreach (DataRow r in dt.Rows)
                Series.Add(new Series((string)r["Name"], (long)r["id"]));

            return true;
        }

        public bool LoadDiscSets()
        {
            if (_data == null)
                return false;

            DataTable dt = _data.GetTable("DiscSet");
            DiscSets = new List<DiscSet>();
            foreach (DataRow r in dt.Rows)
                DiscSets.Add(new DiscSet(
                                (string)r["Name"],
                                (long)r["DiscCount"],
                                (string)r["Source"],
                                Regions.Find(reg => reg.ID == (long)r["Region"]),
                                Series.Find(s => s.ID == (long)r["Series"]),
                                (long)r["id"]
                ));

            return true;
        }

        public bool LoadDiscs()
        {
            if (_data == null)
                return false;
            
            DataTable dt = _data.GetTable("Disc");
            Discs = new List<Disc>();
            foreach (DataRow r in dt.Rows)
            {
                Discs.Add(new Disc(
                            dt.Columns.Contains("VaultSlotNo") ? (long)r["VaultSlotNo"] : (long)r["id"],
                            (string)r["Name"],
                            (string)r["FriendlyName"],
                            DiscTypes.Find(t => t.ID == (long)r["DiscType"]),
                            DiscSets.Find(s => s.ID == (long)r["DiscSet"]),
                            (long)r["DiscNo"],
                            (long)r["IsBad"] == 1,
                            (string)r["Comment"],
                            (long)r["id"]
                ));
            }

            return true;
        }

        // public int SaveModifiedDiscs()
        // {
        //     List<string[]> values = new List<string[]>();
        //     List<long> ids = new List<long>();

        //     foreach (Disc d in Discs)
        //         if (d.Modified)
        //         {
        //             ids.Add(d.ID);
        //             values.Add(new string[]
        //             {
        //                 d.VaultSlotNo.ToString(),
        //                 d.Name,
        //                 d.FriendlyName,
        //                 d.Type.ID.ToString(),
        //                 d.Set.ID.ToString(),
        //                 d.DiscNo.ToString(),
        //                 d.IsBad ? "1" : "0",
        //                 d.Comment
        //             });
        //         }

        //     string[] columns = new string[]
        //     {
        //         "VaultSlotNo",
        //         "Name",
        //         "FriendlyName",
        //         "DiscType",
        //         "DiscSet",
        //         "DiscNo",
        //         "IsBad",
        //         "Comment"
        //     };
        //     return _data.WriteRows("Disc", columns, values.ToArray(), ids.ToArray());
        // }

        // public int SaveModifiedDiscTypes()
        // {
        //     List<string[]> values = new List<string[]>();
        //     List<long> ids = new List<long>();

        //     foreach (Disc d in Discs)
        //         if (d.Modified)
        //         {
        //             ids.Add(d.ID);
        //             values.Add(new string[]
        //             {
        //                 d.Name
        //             });
        //         }

        //     string[] columns = new string[]
        //     {
        //         "Name"
        //     };
        //     return _data.WriteRows("DiscType", columns, values.ToArray(), ids.ToArray());
        // }

        public int SaveDiscs(List<Disc> discs, bool saveOnlyModified)
        {
            List<string[]> values = new List<string[]>();
            List<long> ids = new List<long>();

            foreach (Disc d in discs)
            {
                if (!d.Modified && saveOnlyModified)
                    continue;

                ids.Add(d.ID);
                values.Add(new string[]
                {
                    d.VaultSlotNo.ToString(),
                    d.Name,
                    d.FriendlyName,
                    d.Type.ID.ToString(),
                    d.Set.ID.ToString(),
                    d.DiscNo.ToString(),
                    d.IsBad.ToString(),
                    d.Comment
                });
            }
            

            string[] columns = new string[]
            {
                "VaultSlotNo",
                "Name",
                "FriendlyName",
                "DiscType",
                "DiscSet",
                "DiscNo",
                "IsBad",
                "Comment"
            };
            
            return _data.WriteRows("Disc", columns, values.ToArray(), ids.ToArray());
        }

        public int SaveDiscSets(List<DiscSet> discSets, bool saveOnlyModified)
        {
            List<string[]> values = new List<string[]>();
            List<long> ids = new List<long>();

            foreach (DiscSet ds in discSets)
            {
                if (!ds.Modified && saveOnlyModified)
                    continue;

                ids.Add(ds.ID);
                values.Add(new string[]
                {
                    ds.Name,
                    ds.DiscCount.ToString(),
                    ds.Source,
                    ds.Region.ID.ToString(),
                    ds.Series.ID.ToString()
                });
            }
            

            string[] columns = new string[]
            {
                "Name",
                "DiscCount",
                "Source",
                "Region",
                "Series"
            };
            
            return _data.WriteRows("DiscSet", columns, values.ToArray(), ids.ToArray());
        }

        public int SaveDiscTypes(List<DiscType> discTypes, bool saveOnlyModified)
        {
            List<string[]> values = new List<string[]>();
            List<long> ids = new List<long>();

            foreach (DiscType dt in discTypes)
            {
                if (!dt.Modified && saveOnlyModified)
                    continue;
                    
                ids.Add(dt.ID);
                values.Add(new string[]
                {
                    dt.Name,
                });
            }
            

            string[] columns = new string[]
            {
                "Name",
            };
            
            return _data.WriteRows("DiscType", columns, values.ToArray(), ids.ToArray());
        }

        public int SaveRegions(List<Region> regions, bool saveOnlyModified)
        {
            List<string[]> values = new List<string[]>();
            List<long> ids = new List<long>();

            foreach (Region r in regions)
            {
                if (!r.Modified && saveOnlyModified)
                    continue;
                    
                ids.Add(r.ID);
                values.Add(new string[]
                {
                    r.Name,
                });
            }
            

            string[] columns = new string[]
            {
                "Name",
            };
            
            return _data.WriteRows("Region", columns, values.ToArray(), ids.ToArray());
        }

        public int SaveSeries(List<Series> series, bool saveOnlyModified)
        {
            List<string[]> values = new List<string[]>();
            List<long> ids = new List<long>();

            foreach (Series s in series)
            {
                if (!s.Modified && saveOnlyModified)
                    continue;
                    
                ids.Add(s.ID);
                values.Add(new string[]
                {
                    s.Name,
                });
            }
            

            string[] columns = new string[]
            {
                "Name",
            };
            
            return _data.WriteRows("Series", columns, values.ToArray(), ids.ToArray());
        }
    }
}