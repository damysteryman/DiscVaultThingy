using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace DiscVaultThingy
{
    public class Data
    {
        private SQLiteConnection dbConn;
        private SQLiteDataAdapter dbAdapter;
        private DataSet ds;

        public Data(string path)
        {
            dbConn = new SQLiteConnection($@"Data Source={path}; Read Only=False;");
        }

        /// <summary>
        /// Method for getting data from database and binding to DataSet
        /// </summary>
        /// <param name="query">SQL query to send to the database</param>
        private DataSet GetDataSet(string query)
        {
            ds = new DataSet();                             // Make new DataSet

            dbAdapter = new SQLiteDataAdapter(query, dbConn);  // Make new DbAdapter using supplied query
            dbConn.Open();                                  // Open connection to database
            dbAdapter.Fill(ds);                             // Fill the DataSet with obtained data
            dbConn.Close();                                 // Close connection to database

            return ds;
        }

        private int SendNonQueries(string[] queries)
        {
            try
            {
                int result = 0;
                dbConn.Open();
                foreach (string q in queries)
                    result += new SQLiteCommand(q, dbConn).ExecuteNonQuery();
                    
                System.Threading.Thread.Sleep(500);
                dbConn.Close();
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        public DataTable GetTable(string tableName)
        {
            string query = $"SELECT * FROM {tableName}";
            return GetDataSet(query).Tables[0];
        }

        public int WriteRow(string table, string[] columns, string[] values, long rowId)
        {
            return SendNonQueries(new string[] { SetRowQuery(table, columns, values, rowId) });
        }

        public int WriteRows(string table, string[] columns, string[][] values, long[] ids)
        {
            if (values.Length != ids.Length)
                throw new Exception("Counts of rows and ids do not match.");

            List<string> queries = new List<string>();

            for (int i = 0; i < values.Length; i++)
                queries.Add(SetRowQuery(table, columns, values[i], ids[i]));

            return SendNonQueries(queries.ToArray());
        }

        private string SetRowQuery(string table, string[] columns, string[] values, long rowId)
        {
            if (columns.Length != values.Length)
                throw new Exception("Counts of columns and values do not match.");
            
            StringBuilder query = new StringBuilder();
            if (rowId > 0)  // existing record
            {
                query.Append($"UPDATE {table} SET ");
                string[] settings = new string[columns.Length];
                for (int i = 0; i < columns.Length; i++)
                    settings[i] = $"`{columns[i]}`='{values[i].Replace("'","''")}'";
                query.AppendJoin(',', settings);
                query.Append($" WHERE id={rowId}");
            }
            else            // new record
            {
                query.Append($"INSERT INTO {table} (");
                query.AppendJoin(',', columns.Select(s => s = $"`{s}`"));
                query.Append(") VALUES (");
                query.AppendJoin(',', values.Select(s => s = $"'{s.Replace("'","''")}'"));
                query.Append(")");
            }

            return query.ToString();
        }
    }
}