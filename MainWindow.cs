using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace DiscVaultThingy
{
    class MainWindow : Window
    {
        #region UI Widgets

        #region MainWindow
        [UI] private Notebook nbRecords = null;
        [UI] private SearchEntry seFilterDisc = null;
        [UI] private SearchEntry seFilterDiscSet = null;
        [UI] private SearchEntry seFilterDiscType = null;
        [UI] private SearchEntry seFilterRegion = null;
        [UI] private SearchEntry seFilterSeries = null;
        TreeModelSort tmsDiscSetCbx = null;
        TreeModelSort tmsDiscTypeCbx = null;
        TreeModelSort tmsRegionCbx = null;
        TreeModelSort tmsSeriesCbx = null;
        #endregion

        #region Disc
        [UI] private ListStore lsDisc = null;
        [UI] private TreeModelFilter tmfDisc = null;
        [UI] private TreeModelSort tmsDisc = null;
        [UI] private TreeSelection tsDisc = null;
        [UI] private TreeView tvDisc = null;
        [UI] private TreeViewColumn tcDiscID = null;
        [UI] private TreeViewColumn tcDiscVaultSlotNo = null;
        [UI] private TreeViewColumn tcDiscName = null;
        [UI] private TreeViewColumn tcDiscFriendlyName = null;
        [UI] private TreeViewColumn tcDiscDiscType = null;
        [UI] private TreeViewColumn tcDiscDiscSet = null;
        [UI] private TreeViewColumn tcDiscDiscNo = null;
        [UI] private TreeViewColumn tcDiscIsBad = null;
        [UI] private TreeViewColumn tcDiscComment = null;
        [UI] private CellRenderer crDiscID = null;
        [UI] private CellRendererText crDiscVaultSlotNo = null;
        [UI] private CellRendererText crDiscName = null;
        [UI] private CellRendererText crDiscFriendlyName = null;
        [UI] private CellRendererCombo crDiscDiscType = null;
        [UI] private CellRendererCombo crDiscDiscSet = null;
        [UI] private CellRendererText crDiscDiscNo = null;
        [UI] private CellRendererToggle crDiscIsBad = null;
        [UI] private CellRendererText crDiscComment = null;
        
        #endregion

        #region DiscSet
        [UI] private ListStore lsDiscSet = null;
        [UI] private TreeSelection tsDiscSet = null;
        [UI] private TreeModelFilter tmfDiscSet = null;
        [UI] private TreeModelSort tmsDiscSet = null;
        [UI] private TreeView tvDiscSet = null;
        [UI] private TreeViewColumn tcDiscSetID = null;
        [UI] private TreeViewColumn tcDiscSetName = null;
        [UI] private TreeViewColumn tcDiscSetDiscCount = null;
        [UI] private TreeViewColumn tcDiscSetSource = null;
        [UI] private TreeViewColumn tcDiscSetRegion = null;
        [UI] private TreeViewColumn tcDiscSetSeries = null;
        [UI] private CellRenderer crDiscSetID = null;
        [UI] private CellRendererText crDiscSetName = null;
        [UI] private CellRendererText crDiscSetDiscCount = null;
        [UI] private CellRendererText crDiscSetSource = null;
        [UI] private CellRendererCombo crDiscSetRegion = null;
        [UI] private CellRendererCombo crDiscSetSeries = null;
        #endregion

        #region Series
        [UI] private ListStore lsSeries = null;
        [UI] private TreeModelFilter tmfSeries = null;
        [UI] private TreeModelSort tmsSeries = null;
        [UI] private TreeSelection tsSeries = null;
        [UI] private TreeView tvSeries = null;
        [UI] private TreeViewColumn tcSeriesID = null;
        [UI] private TreeViewColumn tcSeriesName = null;
        [UI] private CellRenderer crSeriesID = null;
        [UI] private CellRendererText crSeriesName = null;
        #endregion

        #region Region
        [UI] private ListStore lsRegion = null;
        [UI] private TreeModelFilter tmfRegion = null;
        [UI] private TreeModelSort tmsRegion = null;
        [UI] private TreeSelection tsRegion = null;
        [UI] private TreeView tvRegion = null;
        [UI] private TreeViewColumn tcRegionID = null;
        [UI] private TreeViewColumn tcRegionName = null;
        [UI] private CellRenderer crRegionID = null;
        [UI] private CellRendererText crRegionName = null;
        #endregion

        #region DiscType
        [UI] private ListStore lsDiscType = null;
        [UI] private TreeModelFilter tmfDiscType = null;
        [UI] private TreeModelSort tmsDiscType = null;
        [UI] private TreeSelection tsDiscType = null;
        [UI] private TreeView tvDiscType = null;
        [UI] private TreeViewColumn tcDiscTypeID = null;
        [UI] private TreeViewColumn tcDiscTypeName = null;
        [UI] private CellRenderer crDiscTypeID = null;
        [UI] private CellRendererText crDiscTypeName = null;
        #endregion

        #endregion
        
        #region Globals
        Vault VAULT = new Vault("VAULT-skeleton.sqlite");
        #endregion

        #region Constructor
        public MainWindow() : this(new Builder("MainWindow.glade")) { }

        private MainWindow(Builder builder) : base(builder.GetObject("MainWindow").Handle)
        {
            builder.Autoconnect(this);

            DeleteEvent += Window_DeleteEvent;

            VAULT.LoadData();
            InitTreeViews();
            LoadDB();
        }
        #endregion

        #region Events

        #region MainWindow
        private void Window_DeleteEvent(object sender, DeleteEventArgs a) => Application.Quit();
        private void seFilterDisc_Changed(object sender, EventArgs a) => tmfDisc.Refilter();
        private void seFilterDiscSet_Changed(object sender, EventArgs a) => tmfDiscSet.Refilter();
        private void seFilterDiscType_Changed(object sender, EventArgs a) => tmfDiscType.Refilter();
        private void seFilterRegion_Changed(object sender, EventArgs a) => tmfRegion.Refilter();
        private void seFilterSeries_Changed(object sender, EventArgs a) => tmfSeries.Refilter();
        private void nbRecords_SwitchPage(object sender, SwitchPageArgs a) => SwitchFilterBoxes(a.PageNum);
        private void btnNewRecord_Clicked(object sender, EventArgs a)
        {
            switch(nbRecords.Page)
            {
                case 0:
                    AddNewDisc();
                    break;

                case 1:
                    AddNewDiscSet();
                    break;

                case 2:
                    AddNewSeries();
                    break;

                case 3:
                    AddNewRegion();
                    break;

                case 4:
                    AddNewDiscType();
                    break;
            }
        }
        private void btnSave_Clicked(object sender, EventArgs a)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Saved ");
            switch(nbRecords.Page)
            {
                case 0:
                    sb.Append($"{VAULT.SaveDiscs(VAULT.Discs, true)} Discs.");
                    break;

                case 1:
                    sb.Append($"{VAULT.SaveDiscSets(VAULT.DiscSets, true)} DiscSets.");
                    break;

                case 2:
                    sb.Append($"{VAULT.SaveSeries(VAULT.Series, true)} Series.");
                    break;

                case 3:
                    sb.Append($"{VAULT.SaveRegions(VAULT.Regions, true)} Regions.");
                    break;

                case 4:
                    sb.Append($"{VAULT.SaveDiscTypes(VAULT.DiscTypes, true)} DiscsTypes.");
                    break;
            }
            Console.WriteLine(sb.ToString());
        }
        #endregion

        #region Disc
        private void tcDiscColHeader_Clicked(object sender, EventArgs a) => SortDiscs((TreeViewColumn)sender);
        private void crDiscVaultSlotNo_Edited(object sender, EditedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscVaultSlotNo), a);
        private void crDiscName_Edited(object sender, EditedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscName), a);
        private void crDiscFriendlyName_Edited(object sender, EditedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscFriendlyName), a);
        private void crDiscDiscType_Changed(object sender, ChangedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscDiscType), a);
        private void crDiscDiscSet_Changed(object sender, ChangedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscDiscSet), a);
        private void crDiscDiscNo_Edited(object sender, EditedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscDiscNo), a);
        private void crDiscIsBad_Toggled(object sender, ToggledArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscIsBad), a);
        private void crDiscComment_Edited(object sender, EditedArgs a) => UpdateDisc(tmsDisc, nameof(tcDiscComment), a);
        #endregion

        #region DiscSet
        private void tcDiscSetColHeader_Clicked(object sender, EventArgs a) => SortDiscSets((TreeViewColumn)sender);
        private void crDiscSetName_Edited(object sender, EditedArgs a) => UpdateDiscSet(tmsDiscSet, nameof(tcDiscSetName), a);
        private void crDiscSetDiscCount_Edited(object sender, EditedArgs a) => UpdateDiscSet(tmsDiscSet, nameof(tcDiscSetDiscCount), a);
        private void crDiscSetSource_Edited(object sender, EditedArgs a) => UpdateDiscSet(tmsDiscSet, nameof(tcDiscSetSource), a);
        private void crDiscSetRegion_Changed(object sender, ChangedArgs a) => UpdateDiscSet(tmsDiscSet, nameof(tcDiscSetRegion), a);
        private void crDiscSetSeries_Changed(object sender, ChangedArgs a) => UpdateDiscSet(tmsDiscSet, nameof(tcDiscSetSeries), a);
        #endregion

        #region DiscType
        private void tcDiscTypeColHeader_Clicked(object sender, EventArgs a) => SortDiscTypes((TreeViewColumn)sender);
        private void crDiscTypeName_Edited(object sender, EditedArgs a) => UpdateDiscType(tmsDiscType, nameof(tcDiscTypeName), a);
        #endregion

        #region Region
        private void tcRegionColHeader_Clicked(object sender, EventArgs a) => SortRegions((TreeViewColumn)sender);
        private void crRegionName_Edited(object sender, EditedArgs a) => UpdateRegion(tmsRegion, nameof(tcRegionName), a);
        #endregion

        #region Series
        private void tcSeriesColHeader_Clicked(object sender, EventArgs a) => SortSeries((TreeViewColumn)sender);
        private void crSeriesName_Edited(object sender, EditedArgs a) => UpdateSeries(tmsSeries, nameof(tcSeriesName), a);
        #endregion

        #endregion

        #region TreeView Data Editors
        private void UpdateDisc(TreeModelSort tms, string colName, GLib.SignalArgs a)
        {
            TreeIter sortedIter;
            tms.GetIterFromString(out sortedIter, a.Args[0].ToString());
            TreeIter iter = (tms.Model as TreeModelFilter).ConvertIterToChildIter(tms.ConvertIterToChildIter(sortedIter));
            ListStore ls = (ListStore)(tms.Model as TreeModelFilter).Model;

            Disc d = (Disc)ls.GetValue(iter, 0);
            StringBuilder sb = new StringBuilder($"Disc ID: {d.ID}\t");

            switch(colName)
            {
                case "tcDiscVaultSlotNo":
                    long dsl;

                    if (!long.TryParse(a.Args[1].ToString(), out dsl) || d.VaultSlotNo == dsl)
                        return;
                    
                    sb.Append($"{nameof(d.VaultSlotNo)}:\t'{d.VaultSlotNo}' => '{dsl}'");
                    d.VaultSlotNo = dsl;
                    break;

                case "tcDiscName":
                    string dn = a.Args[1].ToString();

                    if (d.Name == dn)
                        return;

                    sb.Append($"{nameof(d.Name)}:\t'{d.Name}' => '{dn}'");
                    d.Name = dn;
                    break;
                    

                case "tcDiscFriendlyName":
                    string dfn = ((EditedArgs)a).NewText;

                    if (d.FriendlyName == dfn)
                        return;

                    sb.Append($"{nameof(d.FriendlyName)}:\t\'{d.FriendlyName}' => '{dfn}'");
                    d.FriendlyName = dfn;
                    break;

                case "tcDiscDiscType":
                    TreeIter dtIter = tmsDiscTypeCbx.ConvertIterToChildIter((TreeIter)(a.Args[1]));
                    DiscType dt = (DiscType)lsDiscType.GetValue(dtIter, 1);

                    if (d.Type.ID == dt.ID)
                        return;

                    sb.Append($"{nameof(d.Type)}:\t\'{d.Type.Name}' => '{dt.Name}'");
                    d.Type = dt;
                    break;

                case "tcDiscDiscSet":
                    TreeIter dsIter = tmsDiscSetCbx.ConvertIterToChildIter((TreeIter)(a.Args[1]));
                    DiscSet ds = (DiscSet)lsDiscSet.GetValue(dsIter, 1);

                    if (d.Set.ID == ds.ID)
                        return;

                    sb.Append($"{nameof(d.Set)}:\t\'{d.Set.Name}' => '{ds.Name}'");
                    d.Set = ds;
                    break;

                case "tcDiscDiscNo":
                    long dno;
                    if (!long.TryParse(a.Args[1].ToString().Split("/")[0].Trim(), out dno) || d.DiscNo == dno)
                        return;

                    sb.Append($"{nameof(d.DiscNo)}:\t\'{d.DiscNo}' => '{dno}'");
                    d.DiscNo = dno;

                    break;

                case "tcDiscIsBad":
                    bool dib = !d.IsBad;
                    sb.Append($"{nameof(d.IsBad)}:\t'{d.IsBad.ToString()}' => '{dib.ToString()}'");
                    d.IsBad = dib;
                    break;

                case "tcDiscComment":
                    string dc = ((EditedArgs)a).NewText;

                    if (d.Comment == dc)
                        return;

                    sb.Append($"{nameof(d.Comment)}:\t'{d.Comment}' => '{dc}'");
                    d.Comment = dc;
                    break;
            }
            ls.SetValue(iter, 0, d);
            Console.WriteLine(sb.ToString());
        }
        private void UpdateDiscSet(TreeModelSort tms, string colName, GLib.SignalArgs a)
        {
            TreeIter sortedIter;
            tms.GetIterFromString(out sortedIter, a.Args[0].ToString());
            TreeIter iter = (tms.Model as TreeModelFilter).ConvertIterToChildIter(tms.ConvertIterToChildIter(sortedIter));
            ListStore ls = (ListStore)(tms.Model as TreeModelFilter).Model;

            DiscSet ds = (DiscSet)ls.GetValue(iter, 1);
            StringBuilder sb = new StringBuilder($"DiscSet ID: {ds.ID}\t");

            switch(colName)
            {
                case "tcDiscSetName":
                    string dn = a.Args[1].ToString();
                    if (ds.Name == dn)
                        return;
                    sb.Append($"{nameof(ds.Name)}:\t'{ds.Name}' => '{dn}'");
                    ds.Name = dn;
                    break;

                case "tcDiscSetDiscCount":
                    long dc;
                    if (!long.TryParse(a.Args[1].ToString(), out dc) || ds.DiscCount == dc)
                        return;

                    sb.Append($"{nameof(ds.Name)}:\t'{ds.DiscCount}' => '{dc}'");
                    ds.DiscCount = dc;
                    break;

                case "tcDiscSetSource":
                    string dsc = a.Args[1].ToString();

                    if (ds.Source == dsc)
                        return;

                    sb.Append($"{nameof(ds.Source)}:\t\'{ds.Source}' => '{dsc}'");
                    ds.Source = dsc;
                    break;

                case "tcDiscSetRegion":
                    TreeIter rIter = tmsRegionCbx.ConvertIterToChildIter((TreeIter)(a.Args[1]));
                    Region r = (Region)lsRegion.GetValue(rIter, 1);

                    if (ds.Region.ID == r.ID)
                        return;

                    sb.Append($"{nameof(ds.Region)}:\t\'{ds.Region.Name}' => '{r.Name}'");
                    ds.Region = r;
                    break;

                case "tcDiscSetSeries":
                    TreeIter sIter = tmsSeriesCbx.ConvertIterToChildIter((TreeIter)(a.Args[1]));
                    Series s = (Series)lsSeries.GetValue(sIter, 1);

                    if (ds.Series.ID == s.ID)
                        return;

                    sb.Append($"{nameof(ds.Series)}:\t\'{ds.Series.Name}' => '{s.Name}'");
                    ds.Series = s;
                    break;
            }
            
            ls.SetValue(iter, 1, ds);
            ls.SetValue(iter, 0, JoinSeriesSetName(ds));
            Console.WriteLine(sb.ToString());
        }   
        private void UpdateDiscType(TreeModelSort tms, string colName, GLib.SignalArgs a)
        {
            TreeIter sortedIter;
            tms.GetIterFromString(out sortedIter, a.Args[0].ToString());
            TreeIter iter = (tms.Model as TreeModelFilter).ConvertIterToChildIter(tms.ConvertIterToChildIter(sortedIter));
            ListStore ls = (ListStore)(tms.Model as TreeModelFilter).Model;

            DiscType dt = (DiscType)ls.GetValue(iter, 1);
            StringBuilder sb = new StringBuilder($"DiscType ID: {dt.ID}\t");

            switch(colName)
            {
                case "tcDiscTypeName":
                    string dn = a.Args[1].ToString();
                    if (dt.Name == dn)
                        return;
                    sb.Append($"{nameof(dt.Name)}:\t'{dt.Name}' => '{dn}'");
                    dt.Name = dn;
                    break;
            }

            ls.SetValue(iter, 1, dt);
            ls.SetValue(iter, 0, $"{dt.Name}");
            Console.WriteLine(sb.ToString());
        }
        private void UpdateRegion(TreeModelSort tms, string colName, GLib.SignalArgs a)
        {
            TreeIter sortedIter;
            tms.GetIterFromString(out sortedIter, a.Args[0].ToString());
            TreeIter iter = (tms.Model as TreeModelFilter).ConvertIterToChildIter(tms.ConvertIterToChildIter(sortedIter));
            ListStore ls = (ListStore)(tms.Model as TreeModelFilter).Model;

            Region r = (Region)ls.GetValue(iter, 1);
            StringBuilder sb = new StringBuilder($"Region ID: {r.ID}\t");

            switch(colName)
            {
                case "tcRegionName":
                    string dn = a.Args[1].ToString();
                    if (r.Name == dn)
                        return;
                    sb.Append($"{nameof(r.Name)}:\t'{r.Name}' => '{dn}'");
                    r.Name = dn;
                    break;
            }

            ls.SetValue(iter, 1, r);
            ls.SetValue(iter, 0, $"{r.Name}");
            Console.WriteLine(sb.ToString());
        }
        private void UpdateSeries(TreeModelSort tms, string colName, GLib.SignalArgs a)
        {
            TreeIter sortedIter;
            tms.GetIterFromString(out sortedIter, a.Args[0].ToString());
            TreeIter iter = (tms.Model as TreeModelFilter).ConvertIterToChildIter(tms.ConvertIterToChildIter(sortedIter));
            ListStore ls = (ListStore)(tms.Model as TreeModelFilter).Model;

            Series dt = (Series)ls.GetValue(iter, 1);
            StringBuilder sb = new StringBuilder($"Series ID: {dt.ID}\t");

            switch(colName)
            {
                case "tcSeriesName":
                    string dn = a.Args[1].ToString();
                    if (dt.Name == dn)
                        return;
                    sb.Append($"{nameof(dt.Name)}:\t'{dt.Name}' => '{dn}'");
                    dt.Name = dn;
                    break;
            }

            ls.SetValue(iter, 1, dt);
            ls.SetValue(iter, 0, $"{dt.Name}");
            Console.WriteLine(sb.ToString());
        }
        #endregion

        #region TreeView Init + Data Loading/Population Methods
        private void ReloadDiscs()
        {
            lsDisc.Clear();
            foreach (Disc d in VAULT.Discs)
                lsDisc.AppendValues(d);
        }
        private void ReloadDiscSets()
        {
            lsDiscSet.Clear();
            foreach (DiscSet ds in VAULT.DiscSets)
                lsDiscSet.AppendValues(JoinSeriesSetName(ds), ds);
        }
        private void ReloadSeries()
        {
            lsSeries.Clear();
            foreach (Series s in VAULT.Series)
                lsSeries.AppendValues($"{s.Name}", s);
        }
        private void ReloadRegions()
        {
            lsRegion.Clear();
            foreach (Region r in VAULT.Regions)
                lsRegion.AppendValues($"{r.Name}", r);
        }
        private void ReloadDiscTypes()
        {
            lsDiscType.Clear();
            foreach (DiscType dt in VAULT.DiscTypes)
                lsDiscType.AppendValues($"{dt.Name}", dt);
        }
        private void LoadDB()
        {
            ReloadSeries();
            ReloadRegions();
            ReloadDiscTypes();
            ReloadDiscSets();
            ReloadDiscs();
        }
        private void InitTreeViews()
        {
            #region Disc
            lsDisc = new ListStore(typeof(Disc));

            tcDiscID.SetCellDataFunc(crDiscID, new TreeCellDataFunc(RenderDiscID));
            tcDiscVaultSlotNo.SetCellDataFunc(crDiscVaultSlotNo, new TreeCellDataFunc(RenderDiscVaultSlotNo));
            tcDiscName.SetCellDataFunc(crDiscName, new TreeCellDataFunc(RenderDiscName));
            tcDiscFriendlyName.SetCellDataFunc(crDiscFriendlyName, new TreeCellDataFunc(RenderDiscFriendlyName));
            tcDiscDiscType.SetCellDataFunc(crDiscDiscType, new TreeCellDataFunc(RenderDiscDiscType));
            tcDiscDiscSet.SetCellDataFunc(crDiscDiscSet, new TreeCellDataFunc(RenderDiscDiscSet));
            tcDiscDiscNo.SetCellDataFunc(crDiscDiscNo, new TreeCellDataFunc(RenderDiscDiscNo));
            tcDiscIsBad.SetCellDataFunc(crDiscIsBad, new TreeCellDataFunc(RenderDiscIsBad));
            tcDiscComment.SetCellDataFunc(crDiscComment, new TreeCellDataFunc(RenderDiscComment));

            tmfDisc = new TreeModelFilter(lsDisc, null);
            tmfDisc.VisibleFunc = DiscFilter;

            tmsDisc = new TreeModelSort(tmfDisc);
            tmsDisc.SetSortColumnId(0, SortType.Ascending);
            tmsDisc.SetSortFunc(0, SortDiscsByID);

            tvDisc.Model = tmsDisc;

            crDiscVaultSlotNo.Editable = true;
            crDiscName.Editable = true;
            crDiscFriendlyName.Editable = true;
            crDiscDiscSet.Editable = true;
            crDiscDiscType.Editable = true;
            crDiscDiscNo.Editable = true;
            crDiscComment.Editable = true;
            #endregion

            #region DiscSet
            lsDiscSet = new ListStore(typeof(string), typeof(DiscSet));

            tcDiscSetID.SetCellDataFunc(crDiscSetID, new TreeCellDataFunc(RenderDiscSetID));
            tcDiscSetName.SetCellDataFunc(crDiscSetName, new TreeCellDataFunc(RenderDiscSetName));
            tcDiscSetDiscCount.SetCellDataFunc(crDiscSetDiscCount, new TreeCellDataFunc(RenderDiscSetDiscCount));
            tcDiscSetSource.SetCellDataFunc(crDiscSetSource, new TreeCellDataFunc(RenderDiscSetSource));
            tcDiscSetRegion.SetCellDataFunc(crDiscSetRegion, new TreeCellDataFunc(RenderDiscSetRegion));
            tcDiscSetSeries.SetCellDataFunc(crDiscSetSeries, new TreeCellDataFunc(RenderDiscSetSeries));

            tmfDiscSet = new TreeModelFilter(lsDiscSet, null);
            tmfDiscSet.VisibleFunc = DiscSetFilter;

            tmsDiscSet = new TreeModelSort(tmfDiscSet);
            tmsDiscSet.SetSortColumnId(1, SortType.Ascending);
            tmsDiscSet.SetSortFunc(1, SortDiscSetBySeries);

            tvDiscSet.Model = tmsDiscSet;

            crDiscSetName.Editable = true;
            crDiscSetDiscCount.Editable = true;
            crDiscSetSource.Editable = true;
            crDiscSetRegion.Editable = true;
            crDiscSetSeries.Editable = true;
            #endregion

            #region Series
            lsSeries = new ListStore(typeof(string), typeof(Series));

            tcSeriesID.SetCellDataFunc(crSeriesID, new TreeCellDataFunc(RenderSeriesID));
            tcSeriesName.SetCellDataFunc(crSeriesName, new TreeCellDataFunc(RenderSeriesName));

            tmfSeries = new TreeModelFilter(lsSeries, null);
            tmfSeries.VisibleFunc = SeriesFilter;

            tmsSeries = new TreeModelSort(tmfSeries);
            tmsSeries.SetSortColumnId(1, SortType.Ascending);
            tmsSeries.SetSortFunc(1, SortSeriesByID);

            tvSeries.Model = tmsSeries;

            crSeriesName.Editable = true;
            #endregion

            #region Region
            lsRegion = new ListStore(typeof(string), typeof(Region));

            tcRegionID.SetCellDataFunc(crRegionID, new TreeCellDataFunc(RenderRegionID));
            tcRegionName.SetCellDataFunc(crRegionName, new TreeCellDataFunc(RenderRegionName));

            tmfRegion = new TreeModelFilter(lsRegion, null);
            tmfRegion.VisibleFunc = RegionFilter;

            tmsRegion = new TreeModelSort(tmfRegion);
            tmsRegion.SetSortColumnId(1, SortType.Ascending);
            tmsRegion.SetSortFunc(1, SortRegionByID);

            tvRegion.Model = tmsRegion;
            crRegionName.Editable = true;
            #endregion

            #region DiscType
            lsDiscType = new ListStore(typeof(string), typeof(DiscType));

            tcDiscTypeID.SetCellDataFunc(crDiscTypeID, new TreeCellDataFunc(RenderDiscTypeID));
            tcDiscTypeName.SetCellDataFunc(crDiscTypeName, new TreeCellDataFunc(RenderDiscTypeName));

            tmfDiscType = new TreeModelFilter(lsDiscType, null);
            tmfDiscType.VisibleFunc = DiscTypeFilter;

            tmsDiscType = new TreeModelSort(tmfDiscType);
            tmsDiscType.SetSortColumnId(1, SortType.Ascending);
            tmsDiscType.SetSortFunc(1, SortDiscTypeByID);

            tvDiscType.Model = tmsDiscType;
            crDiscTypeName.Editable = true;
            #endregion

            #region ComboBoxes
            tmsDiscSetCbx = new TreeModelSort(lsDiscSet);
            tmsDiscSetCbx.SetSortColumnId(1, SortType.Ascending);
            tmsDiscSetCbx.SetSortFunc(1, SortDiscSetBySeriesPlusName);
            crDiscDiscSet.Model = tmsDiscSetCbx;
            crDiscDiscSet.Mode = CellRendererMode.Editable;
            crDiscDiscSet.TextColumn = 0;
            crDiscDiscSet.HasEntry = false;

            tmsDiscTypeCbx = new TreeModelSort(lsDiscType);
            tmsDiscTypeCbx.SetSortColumnId(1, SortType.Ascending);
            tmsDiscTypeCbx.SetSortFunc(1, SortDiscTypeByName);
            crDiscDiscType.Model = tmsDiscTypeCbx;
            crDiscDiscType.Mode = CellRendererMode.Editable;
            crDiscDiscType.TextColumn = 0;
            crDiscDiscType.HasEntry = false;

            tmsRegionCbx = new TreeModelSort(lsRegion);
            tmsRegionCbx.SetSortColumnId(1, SortType.Ascending);
            tmsRegionCbx.SetSortFunc(1, SortRegionByName);
            crDiscSetRegion.Model = tmsRegionCbx;
            crDiscSetRegion.Mode = CellRendererMode.Editable;
            crDiscSetRegion.TextColumn = 0;
            crDiscSetRegion.HasEntry = false;

            tmsSeriesCbx = new TreeModelSort(lsSeries);
            tmsSeriesCbx.SetSortColumnId(1, SortType.Ascending);
            tmsSeriesCbx.SetSortFunc(1, SortSeriesByName);
            crDiscSetSeries.Model = tmsSeriesCbx;
            crDiscSetSeries.Mode = CellRendererMode.Editable;
            crDiscSetSeries.TextColumn = 0;
            crDiscSetSeries.HasEntry = false;
            #endregion

            SwitchFilterBoxes(0);
        }
        #endregion

        #region Filter Methods
        private void SwitchFilterBoxes(uint pageNum)
        {
            seFilterDisc.Hide();
            seFilterDiscSet.Hide();
            seFilterDiscType.Hide();
            seFilterRegion.Hide();
            seFilterSeries.Hide();
            switch(pageNum)
            {
                case 0:
                    seFilterDisc.Show();
                    break;

                case 1:
                    seFilterDiscSet.Show();
                    break;

                case 2:
                    seFilterSeries.Show();
                    break;

                case 3:
                    seFilterRegion.Show();
                    break;

                case 4:
                    seFilterDiscType.Show();
                    break;
            }
        }
        private bool DiscFilter(ITreeModel model, Gtk.TreeIter iter)
        {
            Disc d = (Disc)model.GetValue(iter, 0);
            string filterTerm = seFilterDisc.Text;

            if (d == null || string.IsNullOrEmpty(filterTerm))
                return true;
            else
                return  d.VaultSlotNo.ToString().Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        d.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        d.FriendlyName.Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        d.Type.Name.Contains(seFilterDisc.Text, StringComparison.OrdinalIgnoreCase) ||
                        d.Set.Series.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        d.DiscNo.ToString().Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        d.Comment.Contains(filterTerm, StringComparison.OrdinalIgnoreCase);
        }
        private bool DiscSetFilter(ITreeModel model, Gtk.TreeIter iter)
        {
            DiscSet ds = (DiscSet)model.GetValue(iter, 1);
            string filterTerm = seFilterDiscSet.Text;

            if (ds == null || string.IsNullOrEmpty(filterTerm))
                return true;
            else
                return  ds.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        ds.DiscCount.ToString().Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        ds.Source.Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        ds.Region.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase) ||
                        ds.Series.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase);
        }
        private bool RegionFilter(ITreeModel model, Gtk.TreeIter iter)
        {
            Region r = (Region) model.GetValue(iter, 1);
            string filterTerm = seFilterRegion.Text;

            if (r == null || string.IsNullOrEmpty(filterTerm))
                return true;
            else
                return r.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase);
        }
        private bool SeriesFilter(ITreeModel model, Gtk.TreeIter iter)
        {
            Series s = (Series) model.GetValue(iter, 1);
            string filterTerm = seFilterSeries.Text;

            if (s == null || string.IsNullOrEmpty(filterTerm))
                return true;
            else
                return s.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase);
        }
        private bool DiscTypeFilter(ITreeModel model, Gtk.TreeIter iter)
        {
            DiscType dt = (DiscType) model.GetValue(iter, 1);
            string filterTerm = seFilterDiscType.Text;

            if (dt == null || string.IsNullOrEmpty(filterTerm))
                return true;
            else
                return dt.Name.Contains(filterTerm, StringComparison.OrdinalIgnoreCase);
        }
        #endregion

        #region Sort Methods
        
        #region Disc
        private void SortDiscs(TreeViewColumn tc)
        {
            TreeModelSort tms = tmsDisc;
            SortType order = tc.SortOrder == SortType.Ascending ? SortType.Descending : SortType.Ascending;
            tc.SortOrder = order;
            tms.SetSortColumnId(0, order);

            switch (tc.Title)
            {
                case "ID":
                    tms.SetSortFunc(0, SortDiscsByID);
                    break;

                case "Slot No.":
                    tms.SetSortFunc(0, SortDiscsByVaultSlotNo);
                    break;

                case "Name":
                    tms.SetSortFunc(0, SortDiscsByName);
                    break;

                case "Friendly Name":
                    tms.SetSortFunc(0, SortDiscsByFriendlyName);
                    break;

                case "Type":
                    tms.SetSortFunc(0, SortDiscsByDiscType);
                    break;

                case "Set":
                    tms.SetSortFunc(0, SortDiscsByDiscSet);
                    break;

                case "No. in Set":
                    tms.SetSortFunc(0, SortDiscsByDiscNo);
                    break;

                case "Is Bad":
                    tms.SetSortFunc(0, SortDiscsByIsBad);
                    break;
                
                case "Comment":
                    tms.SetSortFunc(0, SortDiscsByComment);
                    break;
            }
        }
        private int SortDiscsByID(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return (int)Math.Clamp(da.ID - db.ID, -1, 1);
        }
        private int SortDiscsByVaultSlotNo(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return (int)Math.Clamp(da.VaultSlotNo - db.VaultSlotNo, -1, 1);
        }
        private int SortDiscsByName(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return string.Compare(da.Name, db.Name, true);
        }
        private int SortDiscsByFriendlyName(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return string.Compare(da.FriendlyName, db.FriendlyName, true);
        }
        private int SortDiscsByDiscType(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return string.Compare(da.Type.Name, db.Type.Name, true);
        }
        private int SortDiscsByDiscSet(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return string.Compare(JoinSeriesSetName(da.Set), JoinSeriesSetName(db.Set), true);
        }
        private int SortDiscsByDiscNo(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return (int)Math.Clamp(da.DiscNo - db.DiscNo, -1, 1);
        }
        private int SortDiscsByIsBad(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return Math.Clamp(Convert.ToInt32(da.IsBad) - Convert.ToInt32(db.IsBad), -1, 1);
        }
        private int SortDiscsByComment(ITreeModel model, TreeIter a, TreeIter b)
        {
            Disc da = (Disc)model.GetValue(a, 0);
            Disc db = (Disc)model.GetValue(b, 0);

            if (da == null || db == null)
                return 0;

            return string.Compare(da.Comment, db.Comment, true);
        }
        #endregion

        #region DiscSet
        private void SortDiscSets(TreeViewColumn tc)
        {
            TreeModelSort tms = tmsDiscSet;
            SortType order = tc.SortOrder == SortType.Ascending ? SortType.Descending : SortType.Ascending;
            tc.SortOrder = order;
            tms.SetSortColumnId(1, order);

            switch (tc.Title)
            {
                case "ID":
                    tms.SetSortFunc(1, SortDiscSetByID);
                    break;

                case "Name":
                    tms.SetSortFunc(1, SortDiscSetByName);
                    break;

                case "DiscCount":
                    tms.SetSortFunc(1, SortDiscSetByDiscCount);
                    break;

                case "Source":
                    tms.SetSortFunc(1, SortDiscSetBySource);
                    break;

                case "Region":
                    tms.SetSortFunc(1, SortDiscSetByRegion);
                    break;

                case "Series":
                    tms.SetSortFunc(1, SortDiscSetBySeries);
                    break;
            }
        }
        private int SortDiscSetByID(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return (int)Math.Clamp(dsa.ID - dsb.ID, -1, 1);
        }
        private int SortDiscSetByName(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return string.Compare(dsa.Name, dsb.Name, true);
        }
        private int SortDiscSetBySeriesPlusName(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return string.Compare(JoinSeriesSetName(dsa), JoinSeriesSetName(dsb), true);
        }
        private int SortDiscSetByDiscCount(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return (int)Math.Clamp(dsa.DiscCount - dsb.DiscCount, -1, 1);
        }
        private int SortDiscSetBySource(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return string.Compare(dsa.Source, dsb.Source, true);
        }
        private int SortDiscSetByRegion(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return string.Compare(dsa.Region.Name, dsb.Region.Name, true);
        }
        private int SortDiscSetBySeries(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscSet dsa = (DiscSet)model.GetValue(a, 1);
            DiscSet dsb = (DiscSet)model.GetValue(b, 1);

            if (dsa == null || dsb == null)
                return 0;

            return string.Compare(dsa.Series.Name, dsb.Series.Name, true);
        }
        #endregion

        #region Series
        private void SortSeries(TreeViewColumn tc)
        {
            TreeModelSort tms = tmsSeries;
            SortType order = tc.SortOrder == SortType.Ascending ? SortType.Descending : SortType.Ascending;
            tc.SortOrder = order;
            tms.SetSortColumnId(1, order);

            switch (tc.Title)
            {
                case "ID":
                    tms.SetSortFunc(1, SortSeriesByID);
                    break;

                case "Name":
                    tms.SetSortFunc(1, SortSeriesByName);
                    break;
            }
        }
        private int SortSeriesByID(ITreeModel model, TreeIter a, TreeIter b)
        {
            Series sa = (Series)model.GetValue(a, 1);
            Series sb = (Series)model.GetValue(b, 1);

            if (sa == null || sb == null)
                return 0;

            return (int)Math.Clamp(sa.ID - sb.ID, -1, 1);
        }
        private int SortSeriesByName(ITreeModel model, TreeIter a, TreeIter b)
        {
            Series sa = (Series)model.GetValue(a, 1);
            Series sb = (Series)model.GetValue(b, 1);

            if (sa == null || sb == null)
                return 0;

            return string.Compare(sa.Name, sb.Name, true);
        }
        #endregion

        #region Region
        private void SortRegions(TreeViewColumn tc)
        {
            TreeModelSort tms = tmsRegion;
            SortType order = tc.SortOrder == SortType.Ascending ? SortType.Descending : SortType.Ascending;
            tc.SortOrder = order;
            tms.SetSortColumnId(1, order);

            switch (tc.Title)
            {
                case "ID":
                    tms.SetSortFunc(1, SortRegionByID);
                    break;

                case "Name":
                    tms.SetSortFunc(1, SortRegionByName);
                    break;
            }
        }
        private int SortRegionByID(ITreeModel model, TreeIter a, TreeIter b)
        {
            Region ra = (Region)model.GetValue(a, 1);
            Region rb = (Region)model.GetValue(b, 1);

            if (ra == null || rb == null)
                return 0;

            return (int)Math.Clamp(ra.ID - rb.ID, -1, 1);
        }
        private int SortRegionByName(ITreeModel model, TreeIter a, TreeIter b)
        {
            Region ra = (Region)model.GetValue(a, 1);
            Region rb = (Region)model.GetValue(b, 1);

            if (ra == null || rb == null)
                return 0;

            return string.Compare(ra.Name, rb.Name, true);
        }
        #endregion

        #region DiscType
        private void SortDiscTypes(TreeViewColumn tc)
        {
            TreeModelSort tms = tmsDiscType;
            SortType order = tc.SortOrder == SortType.Ascending ? SortType.Descending : SortType.Ascending;
            tc.SortOrder = order;
            tms.SetSortColumnId(1, order);

            switch (tc.Title)
            {
                case "ID":
                    tms.SetSortFunc(1, SortDiscTypeByID);
                    break;

                case "Name":
                    tms.SetSortFunc(1, SortDiscTypeByName);
                    break;
            }
        }
        private int SortDiscTypeByID(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscType dta = (DiscType)model.GetValue(a, 1);
            DiscType dtb = (DiscType)model.GetValue(b, 1);

            if (dta == null || dtb == null)
                return 0;

            return (int)Math.Clamp(dta.ID - dtb.ID, -1, 1);
        }
        private int SortDiscTypeByName(ITreeModel model, TreeIter a, TreeIter b)
        {
            DiscType dta = (DiscType)model.GetValue(a, 1);
            DiscType dtb = (DiscType)model.GetValue(b, 1);

            if (dta == null || dtb == null)
                return 0;

            return string.Compare(dta.Name, dtb.Name, true);
        }
        #endregion

        #endregion

        #region Column Render Methods

        #region Disc Column Render Methods
        private void RenderDiscID (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = d.ID.ToString();
        }
        private void RenderDiscVaultSlotNo (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = d.VaultSlotNo.ToString();
        }
        private void RenderDiscName (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = d.Name;
        }
        private void RenderDiscFriendlyName (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = d.FriendlyName;
        }
        private void RenderDiscDiscType (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererCombo).Text = d.Type.Name;
        }
        private void RenderDiscDiscSet(TreeViewColumn tree_column, Gtk.CellRenderer cell, Gtk.ITreeModel model, Gtk.TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererCombo).Text = JoinSeriesSetName(d.Set);
        }
        private void RenderDiscDiscNo (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = $"{d.DiscNo.ToString()} / {d.Set.DiscCount}";
        }
        private void RenderDiscIsBad (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererToggle).Active = d.IsBad;
        }
        private void RenderDiscComment (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Disc d = (Disc) model.GetValue (iter, 0);
            (cell as CellRendererText).Text = d.Comment;
        }
        #endregion
        #region DiscSet Column Renderer Methods
        private void RenderDiscSetID (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscSet ds = (DiscSet) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = ds == null ? "NEW" : ds.ID.ToString();
        }
        private void RenderDiscSetName (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscSet ds = (DiscSet) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = ds == null ? "" : ds.Name;
        }
        private void RenderDiscSetDiscCount (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscSet ds = (DiscSet) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = ds == null ? "" : ds.DiscCount.ToString();
        }
        private void RenderDiscSetSource (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscSet ds = (DiscSet) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = ds == null ? "" : ds.Source;
        }
        private void RenderDiscSetRegion (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscSet ds = (DiscSet) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = ds == null ? "" : ds.Region.Name;
        }
        private void RenderDiscSetSeries (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscSet ds = (DiscSet) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = ds == null ? "" : ds.Series.Name;
        }
        #endregion
        #region Series Column Renderer Methods
        private void RenderSeriesID (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Series s = (Series) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = s == null ? "NEW" : s.ID.ToString();
        }
        private void RenderSeriesName (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Series s = (Series) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = s == null ? "" : s.Name;
        }
        #endregion
        #region Region Column Renderer Methods
        private void RenderRegionID (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Region r = (Region) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = r.ID.ToString();
        }
        private void RenderRegionName (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Region r = (Region) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = r.Name;
        }
        #endregion
        #region DiscType Column Renderer Methods
        private void RenderDiscTypeID (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscType d = (DiscType) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = d.ID.ToString();
        }
        private void RenderDiscTypeName (TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            DiscType d = (DiscType) model.GetValue (iter, 1);
            (cell as CellRendererText).Text = d.Name;
        }
        #endregion

        #endregion

        #region New Record Adder Methods
        private void AddNewDisc()
        {
            VAULT.SaveDiscs(new List<Disc>() { new Disc(0, "", "", VAULT.DiscTypes[0], VAULT.DiscSets[0], 0, true, "Uninitialized") }, false);
            VAULT.LoadDiscs();
            ReloadDiscs();
            ScrollToRow(tvDisc, tcDiscName, lsDisc, VAULT.Discs.Count - 1);
        }
        private void AddNewDiscSet()
        {
            VAULT.SaveDiscSets(new List<DiscSet>() { new DiscSet("", 0, "", VAULT.Regions[0], VAULT.Series[0]) }, false);
            VAULT.LoadDiscSets();
            ReloadDiscSets();
            ScrollToRow(tvDiscSet, tcDiscSetName, lsDiscSet, VAULT.DiscSets.Count - 1);
        }
        private void AddNewDiscType()
        {
            VAULT.SaveDiscTypes(new List<DiscType>() { new DiscType("") }, false);
            VAULT.LoadDiscTypes();
            ReloadDiscTypes();
            ScrollToRow(tvDiscType, tcDiscTypeName, lsDiscType, VAULT.DiscTypes.Count - 1);
        }
        private void AddNewRegion()
        {
            VAULT.SaveRegions(new List<Region>() { new Region("") }, false);
            VAULT.LoadRegions();
            ReloadRegions();
            ScrollToRow(tvRegion, tcRegionName, lsRegion, VAULT.Regions.Count - 1);
        }
        private void AddNewSeries()
        {
            VAULT.SaveSeries(new List<Series>() { new Series("") }, false);
            VAULT.LoadSeries();
            ReloadSeries();
            ScrollToRow(tvSeries, tcSeriesName, lsSeries, VAULT.Series.Count - 1);
        }
        #endregion
        
        #region Misc Methods
        private void ScrollToRow(TreeView tv, TreeViewColumn tc, ListStore ls, int row)
        {
            TreeIter iter;
            ls.GetIterFromString(out iter, row.ToString());

            TreePath tp = ls.GetPath(iter);
            tv.ScrollToCell(tp, tc, true, 0, 0);

            tv.Selection.UnselectAll();
            tv.Selection.SelectIter(iter);
        }
        private string JoinSeriesSetName(DiscSet set) => $"{set.Series.Name} | {set.Name} | {set.Source}";
        #endregion
    }
}
